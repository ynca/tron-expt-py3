type = 1 #0 is testing, 1 is main experiment, 2 is localizer (block)

#Input definitions
V_KEY = "2"
A_KEY = "1"
TR_KEY = "5"

import psychopy.monitors

TR_WAIT_MSG = "Waiting for TR trigger from scanner."
RESP_TIME = 2
CHECKERBOARD_FREQ = 10
VIEW_DIST = 57
DATA_DIR_NAME = "data"
MONOSPACE_FONT = "Courier New"
MONOSPACE_FONT_FILENAME = "courier_new.ttf"
NUM_ESC_TO_EXIT = 2

info = {"RunType": ["Main", "Localizer", "Test"], "participant": "", "flipHoriz":True}

dlg = psychopy.gui.DlgFromDict(dictionary=info, title="TRoN",
                                       order=["RunType", "participant", "flipHoriz"])
if not dlg.OK:
    psychopy.core.quit()

if info["RunType"] == "Main":
    type = 1
elif info["RunType"] == "Localizer":
    type = 2
else:
    type = 0

flip = info["flipHoriz"]

PARTICIPANT = info["participant"]

if type == 0:
    EXPERIMENT_NAME = "TRoN_Test"
    FULLSCREEN = False
    MON = psychopy.monitors.Monitor("testMonitor")
    INITIAL_WAIT_TIME = 1
    SOA_TIME = 5
    END_WAIT_TIME = 5
    V_STIM_TIME = [1.0, 1.2, 1.2, 1.8]
    V_STIM_INT = [1, 1, 0.1, 1]
    A_STIM_TIME = [1.0, 1.2, 1.2, 1.8]
    A_STIM_INT = [9.0, 9.0, 3.0, 9.0]
    # V_STIM_TIME = [0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.9, 0.9, 0.9] 
    # V_STIM_INT = [0.01, 0.1, 1, 0.01, 0.1, 1, 0.01, 0.1, 1] 
    # A_STIM_TIME = [0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.9, 0.9, 0.9]
    # A_STIM_INT = [1.0, 3.0, 9.0, 1.0, 3.0, 9.0, 1.0, 3.0, 9.0]
    JITTER = 0.0
    NUM_TRIALS = 4
    INSTRUCTIONS_MSG = """THIS IS JUST A TEST. PRESS ANY KEY."""

elif type == 1:
    EXPERIMENT_NAME = "TRoN_Main"
    FULLSCREEN = True
    MON = psychopy.monitors.Monitor("testMonitor") #REPLACE WITH SCANNER MONITOR CODE
    INITIAL_WAIT_TIME = 20
    SOA_TIME = 10
    END_WAIT_TIME = 10
    V_STIM_TIME = [1.0, 1.2, 1.2, 1.8]*4
    V_STIM_INT = [1, 1, 0.1, 1]*4
    A_STIM_TIME = [1.0, 1.2, 1.2, 1.8]*4
    A_STIM_INT = [9.0, 9.0, 3.0, 9.0]*4
    # V_STIM_TIME = [0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.9, 0.9, 0.9]*2
    # V_STIM_INT = [0.01, 0.1, 1, 0.01, 0.1, 1, 0.01, 0.1, 1]*2
    # A_STIM_TIME = [0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.9, 0.9, 0.9]*2
    # A_STIM_INT = [1.0, 3.0, 9.0, 1.0, 3.0, 9.0, 1.0, 3.0, 9.0]*2
    JITTER = 1
    NUM_TRIALS = 16 #Should yield a 350 second (5:50) experiment
    INSTRUCTIONS_MSG = """You will see flashing checkerboard stimuli and hear complex tones.

    Passively observe the visual stimuli, but respond with your right index finger when you hear a tone.

    Please fixate throughout. Press any key to begin the run."""

else:
    EXPERIMENT_NAME = "TRoN_Localizer"
    FULLSCREEN = True
    MON = psychopy.monitors.Monitor("testMonitor") #REPLACE WITH SCANNER MONITOR CODE
    INITIAL_WAIT_TIME = 12
    SOA_TIME = 12+16
    END_WAIT_TIME = 0
    V_STIM_TIME = [16]*4
    V_STIM_INT = [1]*4
    A_STIM_TIME = [16]*4
    A_STIM_INT = [9.0]*4
    JITTER = 0
    NUM_TRIALS = 4 #Should yield a 236 second (3:56) experiment
    INSTRUCTIONS_MSG = """You will see flashing checkerboard stimuli and hear complex tones.

    Passively observe and listen, maintaining fixation throughout.

    Press any key to begin the run."""