import psychopy
import psychopy.event
import psychopy.visual
mon = psychopy.monitors.Monitor("testMonitor")
mon.setDistance(57)
win = psychopy.visual.Window(fullscr=False, size=(800, 600), monitor=mon)

stim = psychopy.visual.RadialStim(win, tex='sqrXsqr', color=1,size=9, units="deg",
                                      radialCycles=4, angularCycles=18, interpolate=True,
                                      autoLog=False)
stim2 = psychopy.visual.RadialStim(win, tex='sqrXsqr', color=1,size=9, units="deg", ori=9,
                                      radialCycles=4, angularCycles=18, interpolate=True,
                                      autoLog=False)
win.flip()

while True:
    stim.draw()
    win.flip()
    psychopy.event.waitKeys()
    stim2.draw()
    win.flip()
    psychopy.event.waitKeys()

