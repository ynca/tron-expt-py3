import psychopy
import psychopy.gui
import psychopy.data
import psychopy.visual
import psychopy.core
import psychopy.event
import psychopy.sound
import psychopy.monitors
import numpy
import os
import experiment_settings as settings
import csv
import serial
import random
from psychopy import visual, core

class FmriTrial:
    def __init__(self):
        self.exp_info = {"participant": settings.PARTICIPANT}
        self.exp_info["exp_name"] = settings.EXPERIMENT_NAME
        self.exp_info["date"] = psychopy.data.getDateStr()

        source_dir = os.path.dirname(os.path.realpath(__file__))
        data_dir = os.path.join(source_dir, settings.DATA_DIR_NAME)
        self.filename = os.path.join(data_dir, "%s_participant_%s_%s" % (self.exp_info["exp_name"],
                        self.exp_info["participant"], self.exp_info['date']))
        self.csv_filename = self.filename + ".csv"
        self.csv_rows = []

        # Create the data directory if it does not exist
        if not os.path.isdir(data_dir):
            os.makedirs(data_dir)

        # Set up CSV writer
        self.__csv_file = open(self.csv_filename, "w")

        # Set up CSV writer
        self.__csv_file = open(self.csv_filename, "w")
        fieldnames = ["exp_name", "date", "participant", "framerate", "trial_num", "v_start_time", "v_length", "v_intensity", "v_response", "a_start_time", "a_length", "a_intensity", "a_response"]
        self.csv_writer = csv.DictWriter(self.__csv_file, fieldnames=fieldnames)
        self.csv_writer.writeheader()

        # Set up the monitor and the window
        # Get external monitor with psychopy.monitors.getAllMonitors()
        mon = settings.MON
        mon.setDistance(settings.VIEW_DIST)
        self.win = psychopy.visual.Window(fullscr=settings.FULLSCREEN, size=(800, 600), monitor=mon)
        self.win.setMouseVisible(False)

        self.framerate = self.win.getActualFrameRate()

    def __setup_stim(self):

        # Set up stimuli
        self.center_fixation = psychopy.visual.Rect(win=self.win,
                name='center_fixation', units='deg',
                width=[0.1, 0.1][0], height=[0.1, 0.1][1],
                ori=0, pos=[0, 0], lineWidth=0,
                lineColor=[-1]*3, lineColorSpace='rgb',
                fillColor=[-1,-1,-1],
                fillColorSpace='rgb',
                opacity=1,interpolate=True)

        checkerboard_array = numpy.array([[1,-1],
                                          [-1, 1]])
        checkerboard2_array = numpy.array([[-1,1],
                                           [1, -1]])
        self.checkerboard = psychopy.visual.GratingStim(self.win, tex=checkerboard_array, sf=0.25, size=20, units="deg")
        self.checkerboard2 = psychopy.visual.GratingStim(self.win, tex=checkerboard2_array, sf=0.25, size=20, units="deg")
        self.circular_checkerboard_L = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=0.01,size=20, units="deg",
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
        self.circular_checkerboard2_L = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=0.01,size=20, units="deg", ori=9,
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
        self.circular_checkerboard_M = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=0.1,size=20, units="deg",
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
        self.circular_checkerboard2_M = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=0.1,size=20, units="deg", ori=9,
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
        self.circular_checkerboard_H = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=1,size=20, units="deg",
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
        self.circular_checkerboard2_H = psychopy.visual.RadialStim(self.win, tex='sqrXsqr', color=1,contrast=1,size=20, units="deg", ori=9,
                                            radialCycles=4, angularCycles=18, interpolate=True,
                                            autoLog=False)
                                            
        self.instructions_msg = self.__gen_textstim("instructions", settings.INSTRUCTIONS_MSG)

        #Auditory stimulus setup
        sampling_rate = 44100 #in Hz
        stim_duration = 0.05 #in seconds
        ramp_duration = 0.005 #in seconds
        intrastim_duration = stim_duration - 2*ramp_duration #Time between hamming window ramp on and ramp off
        stim_index = numpy.array(range(int(round(stim_duration / (1/float(sampling_rate)))-1)))
        ramp_length = int(ramp_duration / (1/float(sampling_rate)))
        intrastim_length = int(intrastim_duration / (1/float(sampling_rate)))
        ramp_envelope = numpy.array(range(1,ramp_length+1))/float(ramp_length)
        envelope_wave = numpy.concatenate((ramp_envelope, numpy.zeros(intrastim_length)+1, 1-ramp_envelope), axis=0)
        
        RAPitems_array = numpy.array([440,550])
        for i in range(RAPitems_array.size):
            frequency = int(RAPitems_array[i])
            stim_wave = numpy.sin(2*numpy.pi*frequency*(stim_index/float(44100)))
            if stim_wave.size > envelope_wave.size:
                stim_wave = stim_wave[range(envelope_wave.size)]
            if stim_wave.size < envelope_wave.size:
                stim_wave = numpy.concatenate(stim_wave, numpy.zeros(envelope_wave.size - stim_wave.size))
            stim_wave_with_envelope = stim_wave*envelope_wave
            if i == 0:
                stim_stream = stim_wave_with_envelope
            else:
                stim_stream = numpy.concatenate((stim_stream, stim_wave_with_envelope), axis=0)
        
        self.sound = psychopy.sound.Sound(0.99*stim_stream)


    def __gen_textstim(self, name, text):
        return psychopy.visual.TextStim(win=self.win, name=name, color="black",
                                        bold=True,
                                        font=settings.MONOSPACE_FONT,
                                            text=text, flipHoriz = settings.flip)


    def accept_any_key_or_esc(self, accepted=None, one_less=False):
        """
        Waits for user input and returns a single key.

        If accepted is a list of keys, only listen for keys in accepted.

        Quits if the user mashes Escape.
        """
        num_esc = settings.NUM_ESC_TO_EXIT
        if one_less:
            num_esc -= 1

        escapes_pressed = 0
        while True:
            keys = None
            if accepted is None:
                keys = psychopy.event.waitKeys()
                if keys[0] == "escape":
                    escapes_pressed += 1
                else:
                    return keys[0]
            else:
                keys = psychopy.event.waitKeys(accepted)
                if keys[0] == "escape":
                    escapes_pressed += 1
                elif keys[0] in accepted:
                    return keys[0]


            if escapes_pressed == num_esc:
                self.quit()


    def quit(self):
        psychopy.logging.flush()
        psychopy.core.quit()


    def show_instructions(self):
        self.instructions_msg.draw()
        self.win.flip()


    def wait_for_TR(self):
        self.__gen_textstim("wait_msg", settings.TR_WAIT_MSG).draw()
        self.win.flip()

        esc_count = 0
        while True:
            keys = psychopy.event.getKeys(keyList=[settings.TR_KEY, "escape"])
            if len(keys) > 0:
                if keys[0] == "escape":
                    esc_count += 1
                    if esc_count == settings.NUM_ESC_TO_EXIT:
                        self.save_and_quit()
                else:
                    break


    def show_cb_and_get_keys(self, win, cb1, cb2, freq, duration, start_time):
        psychopy.event.clearEvents() #Clear keyboard events. RTs only recorded during stim presentation itself!
        cycle_length = 1.0 / (freq*2)
        clock = psychopy.core.Clock()
        cycle_clock = psychopy.core.Clock()
        trial_start_time = clock.getTime()
        first_cb = True

        cb1.draw()
        win.flip()

        clock.reset()
        cycle_clock.reset()

        keys_and_timings = []
        esc_count = 0
        while True:
            t = clock.getTime()
            if t > duration:
                break

            if cycle_clock.getTime() > cycle_length:
                if first_cb:
                    first_cb = False
                    cb1.setAutoDraw(False)
                    cb2.draw()
                    win.flip()
                else:
                    first_cb = True
                    cb2.setAutoDraw(False)
                    cb1.draw()
                    win.flip()
                cycle_clock.reset()

            keys = psychopy.event.getKeys(keyList=[settings.V_KEY, settings.A_KEY, "escape"])
            if len(keys) > 0:
                if keys[0] == "escape":
                    esc_count += 1
                    if esc_count == settings.NUM_ESC_TO_EXIT:
                        self.save_and_quit()
                else:
                    RT = clock.getTime() - trial_start_time
                    print (keys[0], RT)
                    keys_and_timings.append({"button_from_box": keys[0], "time":RT})

        self.win.flip()
        psychopy.event.clearEvents()
        return keys_and_timings


    def save_and_quit(self):
        # Store data
        for row in self.csv_rows:
            self.csv_writer.writerow(row)

        os.fsync(self.__csv_file.fileno())
        self.__csv_file.close()
        psychopy.core.quit()


    def run(self):
        self.__setup_stim()

        #Set up trial order
        A_order = list(range(0, settings.NUM_TRIALS))
        V_order = list(range(0, settings.NUM_TRIALS))
        random.shuffle(A_order)
        random.shuffle(V_order)
        
        #Set up jitter times
        jitter = numpy.random.uniform(-1,1,settings.NUM_TRIALS*2)*settings.JITTER
        jitter = numpy.append(jitter, 0)

        self.show_instructions()
        self.accept_any_key_or_esc()
        self.win.flip()

        self.wait_for_TR()
        self.win.flip()

        experiment_clock = psychopy.core.Clock()

        # first, wait 10 s
        self.center_fixation.draw()
        self.win.flip()

        experiment_clock.reset()
        experiment_start = experiment_clock.getTime()

        # Wait
        esc_count = 0
        while experiment_clock.getTime() < settings.INITIAL_WAIT_TIME + jitter[0]:
            keys = psychopy.event.getKeys(keyList=["escape"])
            if len(keys) > 0:
                esc_count += 1
                if esc_count == settings.NUM_ESC_TO_EXIT:
                    self.save_and_quit()

        # start the trials

        timelimit = settings.INITIAL_WAIT_TIME + jitter[0] # avoid inaccuracy for the first round
        for i in range(0, settings.NUM_TRIALS):
            trial_data = {}
            esc_count = 0
            start_time = experiment_clock.getTime()
                        
            trial_tasks = [{"duration": settings.V_STIM_TIME[V_order[i]], "intensity": settings.V_STIM_INT[V_order[i]], "stage": "v_stim"},
                {"duration": settings.SOA_TIME - settings.V_STIM_TIME[V_order[i]] - jitter[i*2] + jitter[i*2+1], "intensity": 0, "stage": "wait"},
                {"duration": settings.A_STIM_TIME[A_order[i]], "intensity": settings.A_STIM_INT[A_order[i]], "stage": "a_stim"},
                {"duration": settings.SOA_TIME - settings.A_STIM_TIME[A_order[i]] - jitter[i*2+1] + jitter[i*2+2], "intensity": 0, "stage": "wait"}]
            
            for task in trial_tasks:
                psychopy.event.clearEvents() #Same note as above - Resp only collected during stimulus presentation itself.
                a_responses = []
                audio_played = False
                timelimit += task["duration"]
                if task["stage"] == "v_stim":
                    print ('Vis Stim -> Duration: ' + str(task["duration"]) + ', Intensity: ' + str(task["intensity"]))
                elif task["stage"] == "a_stim":
                    print ('Aud Stim -> Duration: ' + str(task["duration"]) + ', Intensity: ' + str(task["intensity"]/9))
                while True:
                    if experiment_clock.getTime() > timelimit:
                        break
                    else:
                        # Handle visual stimuli
                        if task["stage"] == "v_stim":
                            self.center_fixation.setAutoDraw(False)
                            trial_data["v_start_time"] = experiment_clock.getTime()
                            trial_data["v_length"] = task["duration"]
                            trial_data["v_intensity"] = task["intensity"]
                            if task["intensity"] == 1:
                                keys_and_timings = self.show_cb_and_get_keys(self.win,
                                    self.circular_checkerboard_H, self.circular_checkerboard2_H,
                                    settings.CHECKERBOARD_FREQ, task["duration"], start_time)
                            elif task["intensity"] == 0.1:
                                keys_and_timings = self.show_cb_and_get_keys(self.win,
                                    self.circular_checkerboard_M, self.circular_checkerboard2_M,
                                    settings.CHECKERBOARD_FREQ, task["duration"], start_time)
                            elif task["intensity"] == 0.01:
                                keys_and_timings = self.show_cb_and_get_keys(self.win,
                                    self.circular_checkerboard_L, self.circular_checkerboard2_L,
                                    settings.CHECKERBOARD_FREQ, task["duration"], start_time)
                            trial_data["v_response"] = str(keys_and_timings)
                            psychopy.event.clearEvents()

                        # handle auditary stimuli
                        elif task["stage"] == "a_stim":
                            if not audio_played:
                                trial_data["a_length"] = task["duration"]
                                trial_data["a_intensity"] = task["intensity"]/9
                                trial_data["a_start_time"] = experiment_clock.getTime()
                                self.sound.setVolume(task["intensity"]/9)
                                self.sound.play(loops = int(task["duration"]/0.1))
                                self.center_fixation.setAutoDraw(True)
                                self.win.flip()
                                audio_played = True
                            while True:
                                if experiment_clock.getTime() > trial_data["a_start_time"] + settings.RESP_TIME + task["duration"]:
                                    self.sound.stop()
                                    break
                                else:
                                    keys = psychopy.event.getKeys(keyList=[settings.A_KEY, settings.V_KEY, "escape"])
                                    if len(keys) > 0:
                                        if keys[0] == "escape":
                                            esc_count += 1
                                            if esc_count == settings.NUM_ESC_TO_EXIT:
                                                self.save_and_quit()
                                        else:
                                            t = experiment_clock.getTime() - trial_data["a_start_time"]
                                            print(keys[0], t)
                                            keys_and_timings.append({"button_from_box": keys[0], "time":t})
                            trial_data["a_response"] = str(keys_and_timings)
                            psychopy.event.clearEvents()

                        elif task["stage"] == "wait":
                            self.center_fixation.setAutoDraw(True)
                            self.win.flip()
                            keys = psychopy.event.getKeys(keyList=["escape"])
                            if len(keys) > 0:
                                esc_count += 1
                                if esc_count == settings.NUM_ESC_TO_EXIT:
                                    self.save_and_quit()

                trial_data["exp_name"] = self.exp_info["exp_name"]
                trial_data["date"] = self.exp_info["date"]
                trial_data["participant"] = self.exp_info["participant"]
                trial_data["trial_num"] = i
                trial_data["framerate"] = self.framerate

            self.csv_rows.append(trial_data)

        self.win.flip()
        self.center_fixation.draw()
        self.win.flip()
        psychopy.core.wait(settings.END_WAIT_TIME)

        full_time = experiment_clock.getTime() - experiment_start
        print('Run time (s): ')
        print(full_time)

        self.save_and_quit()
