from psychopy import core, sound, event, clock, visual
import psychopy.monitors
import numpy


#Auditory stimulus setup
sampling_rate = 44100 #in Hz
stim_duration = 0.05 #in seconds
ramp_duration = 0.005 #in seconds
intrastim_duration = stim_duration - 2*ramp_duration #Time between hamming window ramp on and ramp off
stim_index = numpy.array(range(int(round(stim_duration / (1/float(sampling_rate)))-1)))
ramp_length = int(ramp_duration / (1/float(sampling_rate)))
intrastim_length = int(intrastim_duration / (1/float(sampling_rate)))
ramp_envelope = numpy.array(range(1,ramp_length+1))/float(ramp_length)
envelope_wave = numpy.concatenate((ramp_envelope, numpy.zeros(intrastim_length)+1, 1-ramp_envelope), axis=0)

RAPitems_array = numpy.array([440,550])
for i in range(RAPitems_array.size):
    frequency = int(RAPitems_array[i])
    stim_wave = numpy.sin(2*numpy.pi*frequency*(stim_index/float(44100)))
    if stim_wave.size > envelope_wave.size:
        stim_wave = stim_wave[range(envelope_wave.size)]
    if stim_wave.size < envelope_wave.size:
        stim_wave = numpy.concatenate(stim_wave, numpy.zeros(envelope_wave.size - stim_wave.size))
    stim_wave_with_envelope = stim_wave*envelope_wave
    if i == 0:
        stim_stream = stim_wave_with_envelope
    else:
        stim_stream = numpy.concatenate((stim_stream, stim_wave_with_envelope), axis=0)

s = psychopy.sound.Sound(0.99*stim_stream)

win = visual.Window([400,300], monitor="testMonitor")
message = visual.TextStim(win, text='Nothing to see here...')
message.draw()
win.flip()

#intense = [9.0]*9
intense = [9.0, 3.0, 1.0]*3

for i in intense:
    s.setVolume(i/9)
    s.play(loops = 3)
    psychopy.core.wait(2)